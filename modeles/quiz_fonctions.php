<?php

/**
 * Prend un numéro d'image et en fait un texte (image) pour du xml
 *
 * @param int $numero
 * @param bool $echapper_entites
 * @return string
**/
function modeliser_image($numero, $echapper_entites = true) {
	if (!$numero = trim($numero)) return '';

	$modele = "<img$numero>";
	$modele = propre($modele);
	$modele = ptobr($modele);
	$modele = filtrer('image_reduire', $modele, 600, 300);
	$modele = filtrer('image_graver', $modele);
	$modele = trim($modele);
	if ($echapper_entites) {
		$modele = htmlentities($modele);
	}
	return $modele;
}

/**
 * Retourne l'image, mais sans échapper les entités (l'affichera donc)
 *
 * @param int $numero
 * @return string
**/
function modeliser_image_prive($numero) {
	return modeliser_image($numero, false);
}
